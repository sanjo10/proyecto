/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     11/6/2021 15:20:56                           */
/*==============================================================*/


drop index CLIENTE_PK;

drop table CLIENTE;

drop index RELATIONSHIP_8_FK;

drop index RELATIONSHIP_1_FK;

drop index DETALLE_PRODUCTO_PK;

drop table DETALLE_PRODUCTO;

drop index RELATIONSHIP_3_FK;

drop index EMPLEADOS_PK;

drop table EMPLEADOS;

drop index RELATIONSHIP_10_FK;

drop index RELATIONSHIP_9_FK;

drop index RELATIONSHIP_7_FK;

drop index FACTURA_PK;

drop table FACTURA;

drop index RELATIONSHIP_4_FK;

drop index GERENTE_PK;

drop table GERENTE;

drop index RELATIONSHIP_5_FK;

drop index LOCAL_PK;

drop table LOCAL;

drop index RELATIONSHIP_6_FK;

drop index RELATIONSHIP_2_FK;

drop index PRODUCTO_PK;

drop table PRODUCTO;

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE (
   ID_CLIENTE           SERIAL               not null,
   CI_CLIENTE           VARCHAR(10)          not null,
   NOMBRES_CLIENTE      VARCHAR(50)          not null,
   APELLIDOS_CLIENTE    VARCHAR(50)          not null,
   TELF_CLIENTE         VARCHAR(10)          not null,
   constraint PK_CLIENTE primary key (ID_CLIENTE)
);

/*==============================================================*/
/* Index: CLIENTE_PK                                            */
/*==============================================================*/
create unique index CLIENTE_PK on CLIENTE (
ID_CLIENTE
);

/*==============================================================*/
/* Table: DETALLE_PRODUCTO                                      */
/*==============================================================*/
create table DETALLE_PRODUCTO (
   ID_DETALLE           SERIAL               not null,
   ID_FACTURA           INT4                 null,
   ID_PRODUCTO          INT4                 null,
   NOMBRE_PRODUCTO      VARCHAR(50)          not null,
   CANTIDAD_PRODUCTO    INT4                 not null,
   DESCRIPCION_PRODUCTO VARCHAR(200)         not null,
   PRECIO_UNITARIO_PRODUCTO INT4                 not null,
   constraint PK_DETALLE_PRODUCTO primary key (ID_DETALLE)
);

/*==============================================================*/
/* Index: DETALLE_PRODUCTO_PK                                   */
/*==============================================================*/
create unique index DETALLE_PRODUCTO_PK on DETALLE_PRODUCTO (
ID_DETALLE
);

/*==============================================================*/
/* Index: RELATIONSHIP_1_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_1_FK on DETALLE_PRODUCTO (
ID_PRODUCTO
);

/*==============================================================*/
/* Index: RELATIONSHIP_8_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_8_FK on DETALLE_PRODUCTO (
ID_FACTURA
);

/*==============================================================*/
/* Table: EMPLEADOS                                             */
/*==============================================================*/
create table EMPLEADOS (
   ID_EMPLEADOS         SERIAL               not null,
   ID_LOCAL             INT4                 null,
   CI_EMPLEADOS         VARCHAR(10)          not null,
   NOMBRES_EMPLEADOS    VARCHAR(50)          not null,
   APELLIDOS_EMPLEADOS  VARCHAR(50)          not null,
   TEL_EMPLEADO         VARCHAR(10)          not null,
   constraint PK_EMPLEADOS primary key (ID_EMPLEADOS)
);

/*==============================================================*/
/* Index: EMPLEADOS_PK                                          */
/*==============================================================*/
create unique index EMPLEADOS_PK on EMPLEADOS (
ID_EMPLEADOS
);

/*==============================================================*/
/* Index: RELATIONSHIP_3_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_3_FK on EMPLEADOS (
ID_LOCAL
);

/*==============================================================*/
/* Table: FACTURA                                               */
/*==============================================================*/
create table FACTURA (
   ID_FACTURA           SERIAL               not null,
   ID_EMPLEADOS         INT4                 null,
   ID_CLIENTE           INT4                 null,
   ID_LOCAL             INT4                 null,
   NOMBRE_LOCAL         VARCHAR(50)          not null,
   TELF_LOCAL           VARCHAR(10)          not null,
   NOMBRES_EMPLEADOS    VARCHAR(50)          not null,
   APELLIDOS_EMPLEADOS  VARCHAR(50)          not null,
   CI_CLIENTE           VARCHAR(10)          not null,
   NOMBRES_CLIENTE      VARCHAR(50)          not null,
   APELLIDOS_CLIENTE    VARCHAR(50)          not null,
   TELF_CLIENTE         VARCHAR(10)          not null,
   NOMBRE_PRODUCTO      VARCHAR(50)          not null,
   CANTIDAD_PRODUCTO    INT4                 not null,
   PRECIO_UNITARIO_PRODUCTO INT4                 not null,
   DESCRIPCION_PRODUCTO VARCHAR(200)         not null,
   PRECIO_TOTAL         INT4                 not null,
   IVA                  INT4                 not null,
   VALOR_PAGAR          INT4                 not null,
   constraint PK_FACTURA primary key (ID_FACTURA)
);

/*==============================================================*/
/* Index: FACTURA_PK                                            */
/*==============================================================*/
create unique index FACTURA_PK on FACTURA (
ID_FACTURA
);

/*==============================================================*/
/* Index: RELATIONSHIP_7_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_7_FK on FACTURA (
ID_CLIENTE
);

/*==============================================================*/
/* Index: RELATIONSHIP_9_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_9_FK on FACTURA (
ID_EMPLEADOS
);

/*==============================================================*/
/* Index: RELATIONSHIP_10_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_10_FK on FACTURA (
ID_LOCAL
);

/*==============================================================*/
/* Table: GERENTE                                               */
/*==============================================================*/
create table GERENTE (
   ID_GERENTE           SERIAL               not null,
   ID_LOCAL             INT4                 null,
   CI_GERENTE           VARCHAR(10)          not null,
   NOMBRES_GERENTE      VARCHAR(50)          not null,
   APELLIDOS_GERENTE    VARCHAR(102504)      not null,
   TELF_GERENTE         VARCHAR(10)          not null,
   constraint PK_GERENTE primary key (ID_GERENTE)
);

/*==============================================================*/
/* Index: GERENTE_PK                                            */
/*==============================================================*/
create unique index GERENTE_PK on GERENTE (
ID_GERENTE
);

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_4_FK on GERENTE (
ID_LOCAL
);

/*==============================================================*/
/* Table: LOCAL                                                 */
/*==============================================================*/
create table LOCAL (
   ID_LOCAL             SERIAL               not null,
   ID_GERENTE           INT4                 null,
   NOMBRE_LOCAL         VARCHAR(50)          not null,
   UBICACION_LOCAL      VARCHAR(50)          not null,
   CIUDAD_LOCAL         VARCHAR(50)          not null,
   TELF_LOCAL           VARCHAR(10)          not null,
   constraint PK_LOCAL primary key (ID_LOCAL)
);

/*==============================================================*/
/* Index: LOCAL_PK                                              */
/*==============================================================*/
create unique index LOCAL_PK on LOCAL (
ID_LOCAL
);

/*==============================================================*/
/* Index: RELATIONSHIP_5_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_5_FK on LOCAL (
ID_GERENTE
);

/*==============================================================*/
/* Table: PRODUCTO                                              */
/*==============================================================*/
create table PRODUCTO (
   ID_PRODUCTO          SERIAL               not null,
   ID_FACTURA           INT4                 null,
   ID_LOCAL             INT4                 null,
   NOMBRE_PRODUCTO      VARCHAR(50)          not null,
   constraint PK_PRODUCTO primary key (ID_PRODUCTO)
);

/*==============================================================*/
/* Index: PRODUCTO_PK                                           */
/*==============================================================*/
create unique index PRODUCTO_PK on PRODUCTO (
ID_PRODUCTO
);

/*==============================================================*/
/* Index: RELATIONSHIP_2_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_2_FK on PRODUCTO (
ID_LOCAL
);

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_6_FK on PRODUCTO (
ID_FACTURA
);

alter table DETALLE_PRODUCTO
   add constraint FK_DETALLE__RELATIONS_PRODUCTO foreign key (ID_PRODUCTO)
      references PRODUCTO (ID_PRODUCTO)
      on delete restrict on update restrict;

alter table DETALLE_PRODUCTO
   add constraint FK_DETALLE__RELATIONS_FACTURA foreign key (ID_FACTURA)
      references FACTURA (ID_FACTURA)
      on delete restrict on update restrict;

alter table EMPLEADOS
   add constraint FK_EMPLEADO_RELATIONS_LOCAL foreign key (ID_LOCAL)
      references LOCAL (ID_LOCAL)
      on delete restrict on update restrict;

alter table FACTURA
   add constraint FK_FACTURA_RELATIONS_LOCAL foreign key (ID_LOCAL)
      references LOCAL (ID_LOCAL)
      on delete restrict on update restrict;

alter table FACTURA
   add constraint FK_FACTURA_RELATIONS_CLIENTE foreign key (ID_CLIENTE)
      references CLIENTE (ID_CLIENTE)
      on delete restrict on update restrict;

alter table FACTURA
   add constraint FK_FACTURA_RELATIONS_EMPLEADO foreign key (ID_EMPLEADOS)
      references EMPLEADOS (ID_EMPLEADOS)
      on delete restrict on update restrict;

alter table GERENTE
   add constraint FK_GERENTE_RELATIONS_LOCAL foreign key (ID_LOCAL)
      references LOCAL (ID_LOCAL)
      on delete restrict on update restrict;

alter table LOCAL
   add constraint FK_LOCAL_RELATIONS_GERENTE foreign key (ID_GERENTE)
      references GERENTE (ID_GERENTE)
      on delete restrict on update restrict;

alter table PRODUCTO
   add constraint FK_PRODUCTO_RELATIONS_LOCAL foreign key (ID_LOCAL)
      references LOCAL (ID_LOCAL)
      on delete restrict on update restrict;

alter table PRODUCTO
   add constraint FK_PRODUCTO_RELATIONS_FACTURA foreign key (ID_FACTURA)
      references FACTURA (ID_FACTURA)
      on delete restrict on update restrict;

